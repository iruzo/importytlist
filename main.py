import re
import os
from urllib.request import urlopen


def importytlist(api, params):
    patlisttitle = re.compile(
        r'(?<="metadata":\{"playlistMetadataRenderer":\{"title":").+?(?=")')
    paturl = re.compile(
        r'/watch.+?index=\d+?(?=")')
    patvideotitle = re.compile(
        r'(?<="title":\{"runs":\[\{"text":").+?(?="}],"accessibility")')
    # ask for list url
    print("Insert yt list url: ")
    ytlisturl = input()
    with urlopen(ytlisturl) as result:
        html = result.read().decode("utf-8")
        listtitle = re.findall(patlisttitle, html)[0]
        videosubdomains = re.findall(paturl, html)
        titles = re.findall(patvideotitle, html)

        # create list folder if it does not already exists
        if not os.path.exists(api.listpath):
            os.mkdir(api.listpath)

        # create the file and write content
        with open(api.listpath + "/" + listtitle, "w") as listfile:
            for i in range(len(titles)):
                listfile.write(
                    titles[i] + " " + "https://youtube.com" + videosubdomains[i] + "\n")

def main(api, params):
    return importytlist(api, params)
