# Installation
- Insert this repo url in your msailor/config:
```
plug=https://gitlab.com/iruzo/importytlist
```

# Usage
- add yt list to a local list in ~/msailor/list/
- only add first 100 videos of a list
- use the link address https://www.youtube.com/playlist?list=...
